import threading
import socket

class CountryGuideServer:
    '''
    class CountryGuideServer has methods "start" and "handle_client"
    "start" method  is used to start the server side and has two parameters
    :host
    :port
    "handle_lient" is used to handle clients request and respond to them
    '''
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.countries = {
            "Russia":"Moscow",
            "USA":"Washington",
            "France":"Paris",
            "China":"Beijing",
            "Turkmenistan":"Ashgabat",
            "Turkey":"Ankara",
            "Azerbaijan":"Baku",
            "Canada":"Ottawa",
            "Belarus":"Minsk",
            "Columbia":"Bogota",
            "Mexico":"Mexico",
            "Germany":"Berlin",
            "Spain":"Madrid",
            "England":"London",
            "Japan":"Tokio",
            "Scotland":"Glasgow",
            "Czech Republic":"Prague",
            "Belgium":"Brussels",
            "Brazil":"Brasilia",
            "India":"New-Delhi",
            "Poland":"Warsaw",


        }
    def start(self, host, port):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((host, port))   #bind socket with host and port
        self.server_socket.listen(10)  #indicate how much connection socket can take
        print(f"County Guide server is running on, {self.host} : {self.port}")
        while True:
            client_socket, address = self.server_socket.accept()
            print(f"Connected client {address[0]}:{address[1]}")
            client_thread = threading.Thread(target=self.handle_client, args=(client_socket,)) #for multiple clients
            client_thread.start()

    def handle_client(self, client_socket):
        while True:
            country = client_socket.recv(1024).decode() #receive data from clients
            if not country:
                break
            capital = self.countries.get(country) #get capital from dictionary with countires
            if capital:
                client_socket.send(capital.encode())  # send capital of country
            else:
                client_socket.send("County not found".encode()) # if country not in dicitonary

if __name__ == "__main__":
    server = CountryGuideServer("localhost", 5000)  #indicate your IP if you use code not on localhost
    server.start("", 5000)
    server_thread = threading.Thread(target=server.start, args=("localhost, 5000"))
    server_thread.start()
