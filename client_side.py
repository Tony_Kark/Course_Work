import socket
import sys
from datetime import datetime
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit, QPushButton
from sqlalchemy import create_engine, Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base() #Create a base model for a table in a database

class Request(Base):
    '''
    class Request create table with name request
    :column id --> id of user.
    :column client_name --> name of user.
    :column query_time --> time of user request.
    :column query --> request of user.
    '''
    __tablename__ = "requests"
    id = Column(Integer, primary_key=True)
    client_name = Column(String(50))
    query_time = Column(DateTime)
    query = Column(String(100))

class CountryGuideClient(QMainWindow):
    '''
    class CountryGuideClient has a methods "get_capital" and "start".
    "get_capital" method is used for send client query to server side, and receive server response.
     "start" method id used for start client connection to server.
    '''
    def __init__(self, host, port, db_connection):
        super().__init__()
        self.host = host
        self.port = port
        self.db_connection = db_connection

        self.setWindowTitle("Capitals of the Countries") #title of window
        self.setGeometry(100,100,300,200)

        self.label = QLabel("Enter country name:", self) # label with name "Enter country name"
        self.label.setGeometry(20, 20, 260, 30)

        self.input = QLineEdit(self)
        self.input.setGeometry(20, 60, 260, 30)
        self.input.setStyleSheet("background-color:white;font-size: 14px;")

        self.button = QPushButton("Get capital", self) #button which get capital
        self.button.setGeometry(20, 100, 260, 30)
        self.button.setStyleSheet("background-color:#4CAF50;color:white; font-size: 14px;")
        self.button.clicked.connect(self.get_capital)

        self.result_label = QLabel("", self)
        self.result_label.setGeometry(20, 140, 260, 30)
        self.result_label.setStyleSheet("color:white; font-size: 14px:")


    def get_capital(self):
        country = self.input.text().strip() #input country name
        if country:
            self.client_socket.send(country.encode()) #send to server
            capital = self.client_socket.recv(1024).decode() #get server response
            self.result_label.setText(f"Capital is: {capital}") #print response
            self.result_label.setStyleSheet("color:blue; font-size: 14px")
            self.input.clear() #clear entry field after input
            if capital == "County not found":
               self.result_label.setText("Country not found")
               self.result_label.setStyleSheet("color:red; font-size: 14px")

            request = Request(client_name=name, query_time=datetime.now(), query=country) #get data for saving in db
            session = Session() #Session
            session.add(request) #add request to session
            session.commit()  #commit request
            session.close()  #close session


    def start(self):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect((self.host, self.port)) #create connection
        self.show() #to make window visible


    def closeEvent(self, event):
        self.client_socket.close() #close client socket
        event.accept() #accepts a window evet


if __name__ == "__main__":
    '''
    In MySQL Workbench create a database under the name "requests", all data will be save there.
    enter your username, password and database name,
    like this "mysql+pymysql://yourname:yourpassword@localhost/database name"
    '''
    engine = create_engine("mysql+pymysql://username:password@localhost/database name ") #engine for your db
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine) #bind Session to engine
    db_connection = Session #connect db to Session
    name = input("Enter your name:") #get name for save to db, after input open Window
    app = QApplication(sys.argv) #opening
    app.setStyle("Fusion") #Fusion is a style of color, font, size, and other.
    client = CountryGuideClient("localhost", 5000, db_connection) #your host and port
    client.start()
    sys.exit(app.exec_())  #close and exit

